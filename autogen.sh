#!/bin/bash
cd gcc-4.1.2
(cd gcc/dlt/phobos2 && aclocal)
for dir in . gcc intl libssp libmudflap libcpp libiberty fixincludes libphobos; do
  echo "Running autoconf in $dir..."
  (cd $dir && autoconf && rm -rf autom4te.cache);
done
cd libphobos && automake
